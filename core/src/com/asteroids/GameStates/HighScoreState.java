package com.asteroids.GameStates;

import com.asteroids.game.Game;
import com.asteroids.managers.GameStateManager;
import com.asteroids.managers.KeyboardState;
import com.asteroids.managers.Save;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by User on 05.10.14.
 */
public class HighScoreState extends GameState {
    private SpriteBatch spriteBatch;
    private BitmapFont big;
    private BitmapFont normal;
    private long highScores[];
    private String[] names;


    public HighScoreState(GameStateManager gsm) {
        super(gsm);
    }

    @Override
    public void onEnter() {
        spriteBatch = new SpriteBatch();

        //fonts
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("media/fonts/Hyperspace Italic.ttf"));

        parameter.size = 56;
        big = generator.generateFont(parameter);

        generator = new FreeTypeFontGenerator(Gdx.files.internal("media/fonts/Hyperspace Bold Italic.ttf"));
        parameter.size = 30;
        normal = generator.generateFont(parameter);

        Save.load();
        highScores = Save.gameData.getHighScores();
        names = Save.gameData.getNames();

    }

    @Override
    public void render() {
        float width, height;

        spriteBatch.setProjectionMatrix(Game.camera.combined);

        spriteBatch.setColor(Color.WHITE);
        spriteBatch.begin();

        String title = new String("High Scores");

        width = big.getBounds(title).width;

        big.draw(spriteBatch, title, (Game.WIDTH - width) / 2, Game.HEIGHT / 2 + 200);

        for (int i = 0; i < highScores.length; ++i) {
            String highscore = String.format("%02d. %7s %s", i + 1, highScores[i], names[i]);
            width = normal.getBounds(highscore).width;
            normal.draw(spriteBatch, highscore, (Game.WIDTH - width) / 2, Game.HEIGHT / 2 + 150 - i * 35);
        }

        spriteBatch.end();
    }

    @Override
    public void update(float delta) {
        handleInput();
    }

    @Override
    public void afterPause() {
    }

    @Override
    public void onExit() {
    }

    ////////////////////////////////////////////

    private void handleInput() {
        if (KeyboardState.isPressed(Input.Keys.ESCAPE) || KeyboardState.isPressed(Input.Keys.ENTER) || KeyboardState.isPressed(Input.Keys.SPACE)) {
            GSM.changeState(new MenuState(GSM));
        }
    }
}
