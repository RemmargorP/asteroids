package com.asteroids.managers;

import com.asteroids.GameStates.GameState;

import java.util.Vector;

/**
 * Created by User on 06.09.14.
 */
public class GameStateManager {
    //VARS
    private Vector<GameState> gameStates;
    public static enum States{
        MENU,
        PLAY,
        PAUSE
    }
    //////

    public GameStateManager() {
        gameStates = new Vector<GameState>();
    }

    public void update(float delta) {
        gameStates.lastElement().update(delta);
    }
    public void render() {
        gameStates.lastElement().render();
    }
    //////////////////
    //State operations
    public void pushState(GameState gameState) {
        gameStates.addElement(gameState);
        gameStates.lastElement().onEnter();
    }
    public void popState() {
        if (gameStates.size() != 0) {
            gameStates.lastElement().onExit();
            gameStates.removeElementAt(gameStates.size() - 1);
        }
        if (gameStates.size() != 0) {
            gameStates.lastElement().afterPause();
        }
    }
    public void changeState(GameState gameState) {
        popState();
        pushState(gameState);
    }

}
