package com.asteroids.GameStates;

import com.asteroids.managers.GameStateManager;

/**
 * Created by User on 06.09.14.
 */
public abstract class GameState {

    protected GameStateManager GSM;

    protected GameState(GameStateManager GSM) {
        this.GSM = GSM;
    }

    public abstract void onEnter();

    public abstract void render();
    public abstract void update(float delta);

    public abstract void afterPause();
    public abstract void onExit();

}
