package com.asteroids.entities;

import com.asteroids.types.Point;

/**
 * Created by User on 06.09.14.
 */
public abstract class DynamicObject extends GameObject {
    protected Point vector;

    protected float angle;
    protected float speed;
    protected float rotationSpeed;

    public abstract void handleInput();

    public DynamicObject() {
        super();
        vector = new Point();
    }

}
