package com.asteroids.entities;

import com.asteroids.GameStates.PlayState;
import com.asteroids.game.Game;
import com.asteroids.managers.Jukebox;
import com.asteroids.managers.KeyboardState;
import com.asteroids.managers.Save;
import com.asteroids.types.Point;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;

import java.util.ArrayList;

/**
 * Created by User on 07.09.14.
 */
public class Player extends DynamicObject {
    private boolean up;
    private boolean left;
    private boolean right;
    private boolean shoot;

    private boolean alive;
    private boolean hit;
    private final float respawnTime = 1.5f;
    private float hitTime;

    private float maxSpeed;
    private float acceleration;
    private float deceleration;
    private float acceleratingTimer;

    private ArrayList<Bullet> bullets;
    private ArrayList<Point> engineFlame;
    private ArrayList<Particle> particles;

    private long score;
    private int extraLives;
    private long requiredScore;
    private final long deltaRequiredScore = 5000;


    public Player(ArrayList<Bullet> bullets) {
        super();
        init();
        this.bullets = bullets;
    }

    private void init() {
        score = 0;
        extraLives = 2;
        requiredScore = deltaRequiredScore;

        entityID = 1;
        groupID = 1;

        position.x = Game.WIDTH / 2;
        position.y = Game.HEIGHT / 2;

        maxSpeed = 300;
        acceleration = 200;
        deceleration = 10;

        rotationSpeed = 3;

        angle = MathUtils.PI / 2;

        shape = new ArrayList<Point>(4);
        for (int i = 0; i < 4; ++i)
            shape.add(new Point());

        engineFlame = new ArrayList<Point>(3);
        for (int i = 0; i < 3; ++i)
            engineFlame.add(new Point());

        setShape();

        hit = false;
        alive = true;
        hitTime = 0;

        particles = new ArrayList<Particle>();
    }

    public void render(ShapeRenderer sr) {
        sr.setColor(1, 1, 1, 1);
        sr.begin(ShapeRenderer.ShapeType.Line);

        //ship
        if (!hit) {
            for (int i = 0, j = shape.size() - 1; i < shape.size(); j = i++) {
                sr.line(shape.get(i).x, shape.get(i).y, shape.get(j).x, shape.get(j).y);
            }
            //flame
            for (int i = 0, j = engineFlame.size() - 1; i < engineFlame.size(); j = i++) {
                sr.line(engineFlame.get(i).x, engineFlame.get(i).y,
                        engineFlame.get(j).x, engineFlame.get(j).y);
            }
        }


        sr.end();

        //particles
        for (int i = 0; i < particles.size(); ++i) {
            particles.get(i).render(sr);
        }
    }

    public void update(float deltaTime) {
        handleInput();

        //particles

        for (int i = 0; i < particles.size(); ++i) {
            particles.get(i).update(deltaTime);
            if (particles.get(i).shouldRemove()) {
                particles.remove(i);
                i--;
            }
        }

        //check extra lives
        if (score >= requiredScore) {
            ++extraLives;
            requiredScore += deltaRequiredScore;
            Jukebox.play("extraLife");
        }


        //hit
        if (hit) {
            Jukebox.stop("thruster");

            hitTime += deltaTime;
            if (hitTime > respawnTime) {
                if (extraLives != -1)
                    respawn();
            }

            vector.x = vector.y = 0;

            left = right = up = false;

        }


        //turning
        if (left) {
            angle += rotationSpeed * deltaTime;
        } else if (right) {
            angle -= rotationSpeed * deltaTime;
        }

        //accelerating
        if (up) {
            vector.x += MathUtils.cos(angle) * acceleration * deltaTime;
            vector.y += MathUtils.sin(angle) * acceleration * deltaTime;

            acceleratingTimer += deltaTime;

            if (acceleratingTimer > 0.1f) {
                acceleratingTimer = 0;
            }


        } else {
            acceleratingTimer = 0;
        }

        //decelerating
        float vec = (float) Math.sqrt(vector.x * vector.x + vector.y * vector.y);
        if (vec > 0) {
            vector.x -= (vector.x / vec) * deceleration * deltaTime;
            vector.y -= (vector.y / vec) * deceleration * deltaTime;
        }
        if (vec > maxSpeed) {
            vector.x = (vector.x / vec) * maxSpeed;
            vector.y = (vector.y / vec) * maxSpeed;
        }

        //set position
        position.x += vector.x * deltaTime;
        position.y += vector.y * deltaTime;

        //setShape
        setShape();

        //wrap
        wrap();

        if (shoot)
            shoot();

    }


    //////////////////////////////
    private void setShape() {
        setShip();
        setEngineFlame();
    }

    public void handleInput() {
        boolean was_up = up;

        up = KeyboardState.isDown(Input.Keys.UP) || KeyboardState.isDown(Input.Keys.W);
        left = KeyboardState.isDown(Input.Keys.LEFT) || KeyboardState.isDown(Input.Keys.A);
        right = KeyboardState.isDown(Input.Keys.RIGHT) || KeyboardState.isDown(Input.Keys.D);
        shoot = KeyboardState.isPressed(Input.Keys.SPACE);

        if (left && right) {
            left = right = false;
        }

        if (up && !was_up) {
            Jukebox.loop("thruster");
        } else if (!up) {
            Jukebox.stop("thruster");
        }

    }

    private void setShip() {
        shape.get(0).x = position.x + MathUtils.cos(angle) * 8;
        shape.get(0).y = position.y + MathUtils.sin(angle) * 8;

        shape.get(1).x = position.x + MathUtils.cos(angle - 4 * MathUtils.PI / 5) * 8;
        shape.get(1).y = position.y + MathUtils.sin(angle - 4 * MathUtils.PI / 5) * 8;

        shape.get(2).x = position.x + MathUtils.cos(angle + MathUtils.PI) * 5;
        shape.get(2).y = position.y + MathUtils.sin(angle + MathUtils.PI) * 5;

        shape.get(3).x = position.x + MathUtils.cos(angle + 4 * MathUtils.PI / 5) * 8;
        shape.get(3).y = position.y + MathUtils.sin(angle + 4 * MathUtils.PI / 5) * 8;
    }

    private void setEngineFlame() {
        engineFlame.get(0).x = position.x + MathUtils.cos(angle - 5 * MathUtils.PI / 6) * 5;
        engineFlame.get(0).y = position.y + MathUtils.sin(angle - 5 * MathUtils.PI / 6) * 5;

        engineFlame.get(1).x = position.x + MathUtils.cos(angle - MathUtils.PI) * (6 + acceleratingTimer * 50);
        engineFlame.get(1).y = position.y + MathUtils.sin(angle - MathUtils.PI) * (6 + acceleratingTimer * 50);

        engineFlame.get(2).x = position.x + MathUtils.cos(angle + 5 * MathUtils.PI / 6) * 5;
        engineFlame.get(2).y = position.y + MathUtils.sin(angle + 5 * MathUtils.PI / 6) * 5;
    }

    private void shoot() {
        if (!alive)
            return;
        if (bullets.size() == PlayState.MAX_BULLETS)
            return;

        bullets.add(new Bullet(this, new Point(position), angle, 500f, 1f));

        Jukebox.play("shoot");
    }

    public void hit() {
        vector.x = vector.y = 0;

        left = right = up = false;

        if (hit)
            return;

        hit = true;

        alive = false;

        --extraLives;

        int numParticles = MathUtils.random(3, 7);

        for (int i = 0; i < numParticles; ++i)
            particles.add(new Particle(position));

    }

    public void respawn() {
        hit = false;
        hitTime = 0;
        position.x = Game.WIDTH / 2;
        position.y = Game.HEIGHT / 2;
        alive = true;
    }

    public boolean isAlive() {
        return alive;
    }

    public long getScore() {
        return score;
    }

    public int getExtraLives() {
        return extraLives;
    }

    public void loseLife() {
        --extraLives;
    }

    public void incrementScore(long delta) {
        score += delta;
    }

    public void setPosition(Point position) {
        super.setPosition(position);
        setShape();
    }

    public void gameover() {
        Save.gameData.setTentativeScore(score);
        Jukebox.stop("thruster");
    }
}
