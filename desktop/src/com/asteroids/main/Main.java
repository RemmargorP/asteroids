package com.asteroids.main;

import com.asteroids.game.Game;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

/**
 * Created by User on 06.09.14.
 */
public class Main {
    public static void main(String[] args) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

        config.title = "Asteroids";
        config.width = 820;
        config.height = 640;
        config.resizable = false;

        new LwjglApplication(new Game(), config);

    }
}
