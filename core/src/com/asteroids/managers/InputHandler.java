package com.asteroids.managers;

import com.badlogic.gdx.InputAdapter;

/**
 * Created by User on 06.09.14.
 */
public class InputHandler extends InputAdapter{
    public boolean keyDown(int key) {
        KeyboardState.setKey(key, true);
        return true;
    }

    public boolean keyUp(int key) {
        KeyboardState.setKey(key, false);
        return true;
    }


}
