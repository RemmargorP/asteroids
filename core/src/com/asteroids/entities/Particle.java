package com.asteroids.entities;

import com.asteroids.types.Point;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;

/**
 * Created by User on 10.09.14.
 */
public class Particle extends DynamicObject {
    private float timer;
    private boolean remove;

    public Particle(Point pos) {
        position.x = pos.x;
        position.y = pos.y;

        vector.x = MathUtils.random(-2f, 2f);
        vector.y = MathUtils.random(-2f, 2f);

        speed = MathUtils.random(30f, 100f);

        width = MathUtils.random(2, 4);

        timer = MathUtils.random(1f, 3f);

        remove = false;
    }

    @Override
    public void handleInput() {

    }

    @Override
    public void render(ShapeRenderer sr) {
        sr.setColor(1, 1, 1, 1);
        sr.begin(ShapeRenderer.ShapeType.Filled);

        sr.circle(position.x, position.y, width / 2f);

        sr.end();
    }

    @Override
    public void update(float deltaTime) {
        timer -= deltaTime;
        if (timer < 0.0)
            remove = true;

        position.x += vector.x * deltaTime * speed;
        position.y += vector.y * deltaTime * speed;
    }



    ///////////////////////////////////
    public boolean shouldRemove() {
        return remove;
    }
}
