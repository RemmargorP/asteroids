package com.asteroids.entities;

import com.asteroids.types.Point;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;

/**
 * Created by User on 07.09.14.
 */
public class Bullet extends DynamicObject {
    private float lifeTime;
    private boolean remove;

    private DynamicObject from;

    public Bullet(DynamicObject from, Point position, float angle, float speed, float lifeTime) {
        this.from = from;

        this.position = position;
        this.angle = angle;
        this.speed = speed;

        this.lifeTime = lifeTime;

        this.vector.x = MathUtils.cos(angle) * speed;
        this.vector.y = MathUtils.sin(angle) * speed;

        this.width = this.height = 2;

    }

    public void handleInput() {

    }

    public void render(ShapeRenderer shapeRenderer) {
        shapeRenderer.setColor(1, 1, 1, 1);
        //shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        shapeRenderer.circle(position.x, position.y, width / 2);

        shapeRenderer.end();

    }

    public void update(float deltaTime) {
        position.x += vector.x * deltaTime;
        position.y += vector.y * deltaTime;

        wrap();

        lifeTime -= deltaTime;
        if (lifeTime < 0) {
            remove = true;
        }

    }

    ////////////////////////////////

    public boolean shouldRemove() {
        return remove;
    }
}
