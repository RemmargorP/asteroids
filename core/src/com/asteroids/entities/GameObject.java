package com.asteroids.entities;

import com.asteroids.game.Game;
import com.asteroids.types.Point;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.ArrayList;

/**
 * Created by User on 06.09.14.
 */
public abstract class GameObject {
    protected Point position;
    protected int width;
    protected int height;

    protected long entityID;
    protected long groupID;

    protected ArrayList<Point> shape;

    public GameObject() {
        position = new Point();
    }

    public abstract void render(ShapeRenderer sr);

    public abstract void update(float deltaTime);

    protected void wrap() {
        if (position.x < 0)
            position.x = Game.WIDTH;
        if (position.x > Game.WIDTH)
            position.x = 0;

        if (position.y < 0)
            position.y = Game.HEIGHT;
        if (position.y > Game.HEIGHT)
            position.y = 0;
    }


    public boolean containsPoint(Point p) {
        boolean ans = false;
        for (int i = 0, j = shape.size() - 1; i < shape.size(); j = i++) {
            if ((shape.get(i).y > p.y) != (shape.get(j).y > p.y) &&
                    (p.x < (shape.get(j).x - shape.get(i).x) *
                            (p.y - shape.get(i).y) / (shape.get(j).y - shape.get(i).y)
                            + shape.get(i).x)) {
                ans = !ans;
            }
        }

        return ans;
    }

    public boolean intersects(GameObject other) {
        class tmp {
            private float area(Point a, Point b, Point c) {
                return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
            }

            private boolean intersect_1(float a, float b, float c, float d) {
                if (a > b) {
                    float tmp = a;
                    a = b;
                    b = tmp;
                }
                if (c > d) {
                    float tmp = c;
                    c = d;
                    d = tmp;
                }
                return max(a, c) <= min(b, d);
            }

            private float max(float a, float b) {
                if (a >= b)
                    return a;
                return b;
            }

            private float min(float a, float b) {
                if (a <= b)
                    return a;
                return b;
            }

            public boolean intersect(Point a, Point b, Point c, Point d) {
                return intersect_1(a.x, b.x, c.x, d.x)
                        && intersect_1(a.y, b.y, c.y, d.y)
                        && area(a, b, c) * area(a, b, d) <= 0
                        && area(c, d, a) * area(c, d, b) <= 0;
            }
        }

        tmp temp = new tmp();

        boolean inside = false;

        inside = inside || containsPoint(other.getShape().get(0));
        inside = inside || other.containsPoint(shape.get(0));

        if (inside)
            return true;

        for (int x1 = 0, y1 = shape.size() - 1; x1 < shape.size(); y1 = x1++) {
            for (int x2 = 0, y2 = other.getShape().size() - 1; x2 < other.getShape().size(); y2 = x2++) {
                if (temp.intersect(shape.get(x1), shape.get(y1), other.getShape().get(x2), other.getShape().get(y2)))
                    return true;
            }
        }


        return false;
    }


    ///////////////////////////////////////////////////////
    public final float getX() {
        return position.x;
    }

    public final float getY() {
        return position.y;
    }

    public final ArrayList<Point> getShape() {
        return shape;
    }

    public void setPosition(Point position) {
        this.position = new Point(position);
    }
}
