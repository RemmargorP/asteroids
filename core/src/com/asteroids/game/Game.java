package com.asteroids.game;


import com.asteroids.GameStates.LoadingState;
import com.asteroids.GameStates.MenuState;
import com.asteroids.managers.GameStateManager;
import com.asteroids.managers.InputHandler;
import com.asteroids.managers.KeyboardState;
import com.asteroids.managers.Save;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;


public class Game implements ApplicationListener{
	//VARS
    //integers
    public static int WIDTH;
    public static int HEIGHT;
    //graphics
    public static OrthographicCamera camera;
    //Managers
    private GameStateManager GSM;

    private ArrayList<SimpleEntry<String, String>> toLoad;

    public void create() {
        loadMedia();

        WIDTH = Gdx.graphics.getWidth();
        HEIGHT = Gdx.graphics.getHeight();

        camera = new OrthographicCamera(WIDTH, HEIGHT); //set camera
        camera.translate(WIDTH / 2, HEIGHT / 2);
        camera.update();

        Gdx.input.setInputProcessor(new InputHandler());

        GSM = new GameStateManager();
        GSM.pushState(new LoadingState(GSM, new MenuState(GSM), toLoad));

        Save.load();
    }
    public void render() {

        //clear screen to black
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        GSM.update(Gdx.graphics.getDeltaTime());
        GSM.render();

        KeyboardState.update();
    }




    public void resize(int width, int height) {}
    public void dispose() {}
    public void resume() {}
    public void pause() {}

    private void loadMedia() {
        toLoad = new ArrayList<SimpleEntry<String, String>>();
        toLoad.add(new SimpleEntry<String, String>("media/sounds/explode.ogg", "explode"));
        toLoad.add(new SimpleEntry<String, String>("media/sounds/extralife.ogg", "extraLife"));
        toLoad.add(new SimpleEntry<String, String>("media/sounds/largesaucer.ogg", "largeSaucer"));
        toLoad.add(new SimpleEntry<String, String>("media/sounds/pulsehigh.ogg", "pulseHigh"));
        toLoad.add(new SimpleEntry<String, String>("media/sounds/pulselow.ogg", "pulseLow"));
        toLoad.add(new SimpleEntry<String, String>("media/sounds/saucershoot.ogg", "saucerShoot"));
        toLoad.add(new SimpleEntry<String, String>("media/sounds/shoot.ogg", "shoot"));
        toLoad.add(new SimpleEntry<String, String>("media/sounds/smallsaucer.ogg", "smallSaucer"));
        toLoad.add(new SimpleEntry<String, String>("media/sounds/thruster.ogg", "thruster"));
    }

}
