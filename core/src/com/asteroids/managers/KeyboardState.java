package com.asteroids.managers;

/**
 * Created by User on 06.09.14.
 */
public class KeyboardState {
    private static final int NUM_KEYS = 256;
    private static boolean[] keyboard = new boolean[NUM_KEYS];
    private static boolean[] pkeyboard = new boolean[NUM_KEYS];

    public static boolean isDown(int key) {
        return keyboard[key];
    }

    public static boolean isPressed(int key) {
        return keyboard[key] && !pkeyboard[key];
    }

    public static void update() {
        for (int i = 0; i < NUM_KEYS; ++i)
            pkeyboard[i] = keyboard[i];
    }

    public static void setKey(int key, boolean b) {
        keyboard[key] = b;
    }

    ///////////////////////////////////////
}
