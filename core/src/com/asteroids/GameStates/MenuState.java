package com.asteroids.GameStates;

import com.asteroids.game.Game;
import com.asteroids.managers.GameStateManager;
import com.asteroids.managers.KeyboardState;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;


/**
 * Created by User on 04.10.14.
 */
public class MenuState extends GameState {

    private SpriteBatch spriteBatch;
    private BitmapFont titleFont;
    private BitmapFont menuFont;
    private BitmapFont authorFont;

    private final String title = "ASTEROIDS";

    private int curItem;
    private String[] menuItems;

    private boolean up;
    private boolean down;
    private boolean choose;

    public MenuState(GameStateManager gsm) {
        super(gsm);
        init();
    }

    @Override
    public void render() {
        spriteBatch.setProjectionMatrix(Game.camera.combined);

        spriteBatch.setColor(1, 1, 1, 1);
        spriteBatch.begin();

        float width = titleFont.getBounds(title).width;
        titleFont.draw(spriteBatch, title, (Game.WIDTH - width) / 2, Game.HEIGHT / 2 + 150);

        for (int i = 0; i < menuItems.length; ++i) {
            width = menuFont.getBounds(menuItems[i]).width;
            if (curItem == i)
                menuFont.setColor(Color.RED);
            else
                menuFont.setColor(Color.WHITE);
            menuFont.draw(spriteBatch, menuItems[i], (Game.WIDTH - width) / 2, Game.HEIGHT / 2 + 50 - 40 * i);
        }

        authorFont.draw(spriteBatch, "Made by Salavatov Vadim", 30, 30);

        spriteBatch.end();
    }

    @Override
    public void update(float delta) {
        handleInput();

        if (up) {
            --curItem;
            if (curItem == -1)
                ++curItem;
        }
        if (down) {
            ++curItem;
            if (curItem == menuItems.length)
                --curItem;
        }

        if (choose) {
            if (curItem == 0) {
                GSM.changeState(new PlayState(GSM));
            }
            if (curItem == 1) {
                GSM.changeState(new HighScoreState(GSM));
            }
            if (curItem == 2) {
                Gdx.app.exit();
            }

        }


    }

    @Override
    public void afterPause() {

    }

    @Override
    public void onExit() {

    }

    @Override
    public void onEnter() {

    }

    //////////////////////////////////
    private void init() {
        //font
        spriteBatch = new SpriteBatch();

        FreeTypeFontGenerator fontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("media/fonts/Hyperspace Bold.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 70;
        titleFont = fontGenerator.generateFont(parameter);
        parameter.size = 35;
        menuFont = fontGenerator.generateFont(parameter);
        parameter.size = 10;
        authorFont = fontGenerator.generateFont(parameter);


        //menu

        menuItems = new String[]{
                "Play",
                "Highscores",
                "Quit"
        };

    }


    private void handleInput() {
        up = KeyboardState.isPressed(Input.Keys.W) || KeyboardState.isPressed(Input.Keys.UP);
        down = KeyboardState.isPressed(Input.Keys.S) || KeyboardState.isPressed(Input.Keys.DOWN);
        choose = KeyboardState.isPressed(Input.Keys.SPACE);

        if (up && down)
            up = down = false;

    }

}
