package com.asteroids.managers;

import com.badlogic.gdx.Gdx;

import java.io.*;

/**
 * Created by User on 05.10.14.
 */
public class Save {

    public static GameData gameData;

    public static void save() {

        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("asteroids.sav"));
            out.writeObject(gameData);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            Gdx.app.exit();
        }

    }

    public static void load() {
        try {
            if (!saveFileExists()) {
                init();
                return;
            }
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("asteroids.sav"));
            gameData = (GameData) in.readObject();
            in.close();
            gameData.sortHighScores();
        } catch (Exception e) {
            e.printStackTrace();
            Gdx.app.exit();
        }
    }

    public static boolean saveFileExists() {
        File f = new File("asteroids.sav");
        return f.exists();
    }

    public static void init() {
        gameData = new GameData();
        gameData.init();
        save();
    }

}
