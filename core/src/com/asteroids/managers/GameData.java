package com.asteroids.managers;

import java.io.Serializable;

/**
 * Created by User on 05.10.14.
 */
public class GameData implements Serializable {

    private static final long serialVersionUID = 1;

    private final int MAX_SCORES = 10;
    private long[] highScores;
    private String[] names;

    private long tentativeScore;

    public GameData() {
        highScores = new long[MAX_SCORES];
        names = new String[MAX_SCORES];
    }

    public void init() {
        for (int i = 0; i < MAX_SCORES; ++i) {
            highScores[i] = 0;
            names[i] = "-----";
        }

    }

    public long[] getHighScores() {
        return highScores;
    }

    public String[] getNames() {
        return names;
    }

    public long getTentativeScore() {
        return tentativeScore;
    }

    public void setTentativeScore(long value) {
        tentativeScore = value;
    }

    public boolean isHighScore(long score) {
        return score > highScores[MAX_SCORES - 1];
    }

    public void addHighScore(long score, String name) {
        if (!isHighScore(score))
            return;

        highScores[MAX_SCORES - 1] = score;
        names[MAX_SCORES - 1] = name;
        sortHighScores();

    }

    public void sortHighScores() {
        for (int i = MAX_SCORES - 1; i > -1; --i) {
            for (int j = 0; j < i; ++j) {
                if (highScores[j] < highScores[j + 1]) {
                    long tmp1 = highScores[j];
                    highScores[j] = highScores[j + 1];
                    highScores[j + 1] = tmp1;

                    String tmp2 = names[j + 1];
                    names[j + 1] = names[j];
                    names[j] = tmp2;
                }
            }
        }
    }

}
