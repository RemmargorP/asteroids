package com.asteroids.GameStates;

import com.asteroids.game.Game;
import com.asteroids.managers.GameStateManager;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;

/**
 * Created by User on 17.09.14.
 */
public class TransferState extends GameState {
    protected int numChangeColors;
    protected GameState next;
    protected float transferTime;
    protected float transferTimer;
    protected ShapeRenderer SR;
    protected float r, g, b, a;
    protected float nextTimeNewColor;

    public TransferState(GameStateManager gsm, GameState next) {
        super(gsm);
        this.next = next;
        transferTime = 0.5f;
        numChangeColors = 3;
    }
    public TransferState(GameStateManager gsm, GameState next, float transferTime) {
        super(gsm);
        this.next = next;
        this.transferTime = transferTime;
        numChangeColors = 3;
    }
    public TransferState(GameStateManager gsm, GameState next, int numChangeColors) {
        super(gsm);
        this.next = next;
        transferTime = 0.5f;
        this.numChangeColors = numChangeColors;
    }
    public TransferState(GameStateManager gsm, GameState next, float transferTime, int numChangeColors) {
        super(gsm);
        this.next = next;
        this.transferTime = transferTime;
        this.numChangeColors = numChangeColors;
    }

    @Override
    public void onEnter() {
        SR = new ShapeRenderer();
        r = g = b = a = 1f;
        transferTimer = 0f;
        nextTimeNewColor = transferTime / numChangeColors;
    }

    @Override
    public void render() {
        SR.begin(ShapeRenderer.ShapeType.Filled);
        SR.setColor(r, g, b, a);
        SR.rect(0, 0, Game.WIDTH, Game.HEIGHT);
        SR.end();
    }

    @Override
    public void update(float delta) {
        transferTimer += delta;
        if (transferTimer > transferTime) {
            GSM.changeState(next);
        }

        if (transferTimer > nextTimeNewColor) {
            newColor();
            nextTimeNewColor += transferTime / numChangeColors;
        }

    }

    @Override
    public void afterPause() {

    }

    @Override
    public void onExit() {

    }

    /////////////////////////////////

    protected void newColor() {
        r = MathUtils.random(0f, 1f);
        g = MathUtils.random(0f, 1f);
        b = MathUtils.random(0f, 1f);
        a = MathUtils.random(0f, 1f);
    }
    public void setNumChangeColors(int num) {
        numChangeColors = num;
    }
}
