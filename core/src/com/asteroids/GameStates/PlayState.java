package com.asteroids.GameStates;

import com.asteroids.entities.Asteroid;
import com.asteroids.entities.Bullet;
import com.asteroids.entities.Particle;
import com.asteroids.entities.Player;
import com.asteroids.game.Game;
import com.asteroids.managers.GameStateManager;
import com.asteroids.managers.Jukebox;
import com.asteroids.types.Point;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;

import java.util.ArrayList;

/**
 * Created by User on 06.09.14.
 */
public class PlayState extends GameState {
    public final static int MAX_BULLETS = 25;

    private int level;
    private int totalAsteroids;
    private int asteroidsLeft;

    private float backgroundTimer;
    private float maxDelay;
    private float minDelay;
    private float curDelay;
    private boolean playLowPulse;

    private Player player;
    private Player hudPlayer;
    private ShapeRenderer shapeRenderer;
    private SpriteBatch spriteBatch;
    private BitmapFont font;
    private ArrayList<Bullet> bullets;
    private ArrayList<Asteroid> asteroids;
    private ArrayList<Particle> particles;

    public PlayState(GameStateManager gsm) {
        super(gsm);
        init();
    }

    public void onEnter() {

    }

    public void render() {
        player.render(shapeRenderer);

        //asteroids

        for (int i = 0; i < asteroids.size(); ++i) {
            asteroids.get(i).render(shapeRenderer);
        }

        //bullets

        for (int i = 0; i < bullets.size(); ++i) {
            bullets.get(i).render(shapeRenderer);
        }

        //particles

        for (int i = 0; i < particles.size(); ++i) {
            particles.get(i).render(shapeRenderer);
        }

        //score

        spriteBatch.setColor(1, 1, 1, 1);
        spriteBatch.begin();

        font.draw(spriteBatch, "Score: " + Long.toString(player.getScore()), 40, Game.HEIGHT - 30);

        spriteBatch.end();

        //lives

        for (int i = 0; i < player.getExtraLives(); ++i) {
            hudPlayer.setPosition(new Point(40 + i * 15, Game.HEIGHT - 60));
            hudPlayer.render(shapeRenderer);
        }

    }
    public void update(float delta) {
        if (asteroidsLeft == 0) {
            ++level;
            spawnAsteroids();
        }

        player.update(delta);

        for (int i = 0; i < particles.size(); ++i) {
            particles.get(i).update(delta);
            if (particles.get(i).shouldRemove()) {
                particles.remove(i);
                i--;
            }
        }

        for (int i = 0; i < asteroids.size(); ++i) {
            asteroids.get(i).update(delta);
            if (asteroids.get(i).shouldRemove()) {
                asteroids.remove(i);
                i--;

            }
         }

        for (int i = 0; i < bullets.size(); ++i) {
            bullets.get(i).update(delta);
            if (bullets.get(i).shouldRemove()) {
                bullets.remove(i);
                --i;
            }
        }
        checkCollisions();

        //play background music

        backgroundTimer += delta;

        if (player.isAlive() && backgroundTimer >= curDelay) {
            if (playLowPulse) {
                Jukebox.play("pulseLow");
            } else {
                Jukebox.play("pulseHigh");
            }
            playLowPulse = !playLowPulse;
            backgroundTimer = 0;
        }

        if (!player.isAlive() && player.getExtraLives() == -1) {
            player.gameover();
            GSM.changeState(new GameOverState(GSM));
        }

    }

    public void afterPause() {

    }
    public void onExit() {

    }

    //////////////////////////
    private void init() {
        font = new BitmapFont();
        shapeRenderer = new ShapeRenderer();
        spriteBatch = new SpriteBatch();

        bullets = new ArrayList<Bullet>();
        player = new Player(bullets);
        hudPlayer = new Player(null);

        asteroids = new ArrayList<Asteroid>();

        asteroids.add(new Asteroid(new Point(100, 100), Asteroid.LARGE));
        asteroids.add(new Asteroid(new Point(200, 100), Asteroid.MEDIUM));
        asteroids.add(new Asteroid(new Point(300, 100), Asteroid.SMALL));

        level = 1;
        spawnAsteroids();

        particles = new ArrayList<Particle>();

        //set font
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("media/fonts/Hyperspace Bold.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 20;
        font = generator.generateFont(parameter);

        //set up bg music
        maxDelay = 1;
        minDelay = 0.25f;
        curDelay = maxDelay;

        backgroundTimer = maxDelay;

    }
    private void spawnAsteroids() {
        asteroids.clear();

        int numToSpawn = 3 + level;
        Point pos = new Point();
        Point vec = new Point();
        float distance = 0;

        curDelay = maxDelay;

        totalAsteroids = numToSpawn * 7;
        asteroidsLeft = totalAsteroids;

        for (int i = 0; i < numToSpawn; ++i) {
            pos.x = MathUtils.random(Game.WIDTH);
            pos.y = MathUtils.random(Game.HEIGHT);
            vec.x = pos.x - player.getX();
            vec.y = pos.y - player.getY();
            distance = (float) Math.sqrt(vec.x * vec.x + vec.y * vec.y);

            while (distance < 100) {
                pos.x = MathUtils.random(Game.WIDTH);
                pos.y = MathUtils.random(Game.HEIGHT);
                vec.x = pos.x - player.getX();
                vec.y = pos.y - player.getY();
                distance = (float) Math.sqrt(vec.x * vec.x + vec.y * vec.y);
            }

            asteroids.add(new Asteroid(new Point(pos), Asteroid.LARGE));

        }


    }

    private void checkCollisions() {
        //bullet-asteroid
        for (int i = 0; i < bullets.size(); ++i) {
            Bullet b = bullets.get(i);
            for (int j = 0; j < asteroids.size(); ++j) {
                if (asteroids.get(j).containsPoint( new Point( b.getX(), b.getY() ) )) {
                    Asteroid a = asteroids.get(j);

                    bullets.remove(i);
                    --i;

                    player.incrementScore(a.getScore());

                    splitAsteroid(a);
                    asteroids.remove(j);
                    --j;

                    Jukebox.play("explode");
                    break;
                }

            }
        }
        //asteroid-player
        if (player.isAlive()) {
            for (int i = 0; i < asteroids.size(); ++i) {
                if (asteroids.get(i).intersects(player)) {
                    player.hit();

                    splitAsteroid(asteroids.get(i));
                    asteroids.remove(i);
                    --i;

                    Jukebox.play("explode");

                    break;
                }
            }
        }
    }

    private void splitAsteroid(Asteroid a) {
        --asteroidsLeft;
        int type = a.getType();

        curDelay = maxDelay;

        int numParticles = MathUtils.random(type, type + 3);

        for (int i = 0; i < numParticles; ++i)
            particles.add(new Particle(a.getPosition()));

        --type;

        if (type < 0)
            return;

        asteroids.add(new Asteroid(new Point(a.getX(), a.getY()), type));
        asteroids.add(new Asteroid(new Point(a.getX(), a.getY()), type));
    }
}
