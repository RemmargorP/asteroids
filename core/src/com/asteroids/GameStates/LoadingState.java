package com.asteroids.GameStates;

import com.asteroids.game.Game;
import com.asteroids.managers.GameStateManager;
import com.asteroids.managers.Jukebox;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import java.util.AbstractMap;
import java.util.ArrayList;

/**
 * Created by User on 04.10.14.
 */
public class LoadingState extends GameState {

    private SpriteBatch spriteBatch;
    private BitmapFont font;
    private int loaded;
    private ArrayList<AbstractMap.SimpleEntry<String, String>> toLoad;
    private GameState next;

    public LoadingState(GameStateManager gsm, GameState next, ArrayList<AbstractMap.SimpleEntry<String, String>> toLoad) {
        super(gsm);
        this.toLoad = toLoad;
        this.next = next;

        init();
    }

    @Override
    public void render() {

        spriteBatch.setColor(1, 1, 1, 1);
        spriteBatch.begin();

        font.draw(spriteBatch, "LOADING", Game.WIDTH / 2 - 200, Game.HEIGHT / 2 + 50);

        spriteBatch.end();
    }

    @Override
    public void onEnter() {

    }

    @Override
    public void afterPause() {

    }

    @Override
    public void onExit() {

    }

    @Override
    public void update(float delta) {

        if (loaded + 1 == toLoad.size()) {
            GSM.changeState(next);
        }

        Jukebox.load(toLoad.get(loaded).getKey(), toLoad.get(loaded).getValue());
        ++loaded;

    }
    //////////////////////////////////////

    private void init() {
        font = new BitmapFont();
        spriteBatch = new SpriteBatch();

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("media/fonts/Hyperspace Bold.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 100;
        font = generator.generateFont(parameter);

    }

}
