package com.asteroids.types;

/**
 * Created by User on 06.09.14.
 */
public class Point implements Comparable{
    public float x, y;

    public Point() {
        x = 0;
        y = 0;
    }
    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }
    public Point(Point a) {
        x = a.x;
        y = a.y;
    }
    @Override
    public int compareTo(Object obj) {
        Point entry = (Point) obj;

        float res = x - entry.x;

        if (res < 0)
            return -1;
        if (res > 0)
            return 1;

        res = y - entry.y;

        if (res < 0)
            return -1;
        if (res > 0)
            return 1;
        return 0;
    }

}
