package com.asteroids.GameStates;

import com.asteroids.game.Game;
import com.asteroids.managers.GameStateManager;
import com.asteroids.managers.KeyboardState;
import com.asteroids.managers.Save;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by User on 05.10.14.
 */
public class GameOverState extends GameState {

    private SpriteBatch spriteBatch;
    private boolean newHighScore;
    private String title = "GAME OVER";
    private String sNewHighScore[] = {"CONGRATULATIONS!", "NEW HIGH SCORE: "};
    private char[] newName;
    private int curChar;

    private BitmapFont gameOverFont;
    private BitmapFont font;

    public GameOverState(GameStateManager GSM) {
        super(GSM);

    }

    @Override
    public void onEnter() {
        spriteBatch = new SpriteBatch();

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("media/fonts/Hyperspace Bold.ttf"));
        gameOverFont = generator.generateFont(50);
        font = generator.generateFont(30);

        newHighScore = Save.gameData.isHighScore(Save.gameData.getTentativeScore());
        if (!newHighScore) {

        } else {
            newName = new char[]{'A', 'A', 'A', 'A', 'A'};
            curChar = 0;

        }

        sNewHighScore[1] += String.valueOf(Save.gameData.getTentativeScore());

    }

    @Override
    public void render() {
        spriteBatch.setProjectionMatrix(Game.camera.combined);

        spriteBatch.begin();

        float width = gameOverFont.getBounds(title).width;
        gameOverFont.draw(spriteBatch, title, (Game.WIDTH - width) / 2, Game.HEIGHT / 2 + 200);

        if (!newHighScore) {
            spriteBatch.end();
            return;
        }


        for (int i = 0; i < 2; ++i) {
            width = font.getBounds(sNewHighScore[i]).width;
            font.draw(spriteBatch, sNewHighScore[i], (Game.WIDTH - width) / 2, Game.HEIGHT / 2 + 100 - i * 50);
        }

        width = font.getBounds(new String(newName)).width;

        font.draw(spriteBatch, new String(newName), (Game.WIDTH - width) / 2, Game.HEIGHT / 2 - 50);

        font.draw(spriteBatch, "_", (Game.WIDTH - width) / 2 + curChar * 18, Game.HEIGHT / 2 - 60);

        spriteBatch.end();
    }

    @Override
    public void update(float delta) {
        handleInput();
    }

    @Override
    public void afterPause() {

    }

    @Override
    public void onExit() {

    }

    //////////////////////////////////////
    public void handleInput() {
        if (KeyboardState.isPressed(Input.Keys.ENTER) || KeyboardState.isPressed(Input.Keys.ESCAPE) || KeyboardState.isPressed(Input.Keys.SPACE)) {
            if (newHighScore) {
                Save.gameData.addHighScore(Save.gameData.getTentativeScore(), new String(newName));
                Save.save();
            }
            GSM.changeState(new MenuState(GSM));
        }

        if (KeyboardState.isPressed(Input.Keys.UP) || KeyboardState.isPressed(Input.Keys.W)) {
            if (newName[curChar] == '-')
                newName[curChar] = 'A';
            else {
                ++newName[curChar];
                if (newName[curChar] > 'Z')
                    newName[curChar] = '-';
            }
        }

        if (KeyboardState.isPressed(Input.Keys.DOWN) || KeyboardState.isPressed(Input.Keys.S)) {
            if (newName[curChar] == '-')
                newName[curChar] = 'Z';
            else {
                --newName[curChar];
                if (newName[curChar] < 'A')
                    newName[curChar] = '-';
            }
        }

        if (KeyboardState.isPressed(Input.Keys.RIGHT) || KeyboardState.isPressed(Input.Keys.D)) {
            ++curChar;
            if (curChar > newName.length - 1)
                curChar = 0;
        }

        if (KeyboardState.isPressed(Input.Keys.LEFT) || KeyboardState.isPressed(Input.Keys.A)) {
            --curChar;
            if (curChar < 0)
                curChar = newName.length - 1;
        }
    }
}
