package com.asteroids.entities;

import com.asteroids.types.Point;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;

import java.util.ArrayList;

/**
 * Created by User on 07.09.14.
 */
public class Asteroid extends DynamicObject {
    private int type;
    public final static int SMALL = 0;
    public final static int MEDIUM = 1;
    public final static int LARGE = 2;

    private int numPoints;
    private float[] dists;

    private boolean remove;
    private long score;

    public Asteroid(Point position, int type) {
        this.position = position;
        this.type = type;

        init();

    }

    @Override
    public void handleInput() {

    }

    @Override
    public void render(ShapeRenderer shapeRenderer) {
        shapeRenderer.setColor(1, 1, 1, 1);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

        for (int i = 0, j = shape.size() - 1; i < shape.size(); j = i++) {
            shapeRenderer.line(shape.get(i).x, shape.get(i).y, shape.get(j).x, shape.get(j).y);
        }

        shapeRenderer.end();
    }

    @Override
    public void update(float deltaTime) {
        position.x += vector.x * deltaTime;
        position.y += vector.y * deltaTime;

        angle += rotationSpeed * deltaTime;
        setShape();

        wrap();
    }

    ////////////////
    private void init() {
        if (type == SMALL) {
            numPoints = 8;
            width = height = 20;
            speed = MathUtils.random(70, 100);
            score = 80;
        } else if (type == MEDIUM) {
            numPoints = 10;
            width = height = 32;
            speed = MathUtils.random(50, 60);
            score = 50;
        } else {  //LARGE
            numPoints = 12;
            width = height = 50;
            speed = MathUtils.random(20, 30);
            score = 20;
        }

        rotationSpeed = MathUtils.random(-1, 1);
        angle = MathUtils.random(2 * MathUtils.PI);

        vector.x = MathUtils.cos(angle) * speed;
        vector.y = MathUtils.sin(angle) * speed;

        int radius = width / 2;

        dists = new float[numPoints];
        for (int i = 0; i < numPoints; ++i) {
            dists[i] = MathUtils.random(radius / 2, radius);
        }

        shape = new ArrayList<Point>(numPoints);
        for (int i = 0; i < numPoints; ++i)
            shape.add(new Point());
        setShape();

    }

    private void setShape() {
        float tmp_angle = 0;
        for (int i = 0; i < numPoints; ++i) {
            shape.get(i).x = position.x + MathUtils.cos(tmp_angle + angle) * dists[i];
            shape.get(i).y = position.y + MathUtils.sin(tmp_angle + angle) * dists[i];

            tmp_angle += 2 * MathUtils.PI / numPoints;

        }
    }

    public int getType() {
        return type;
    }

    public boolean shouldRemove() {
        return remove;
    }

    public Point getPosition() {
        return new Point(position);
    }

    public long getScore() {
        return score;
    }
}
